# NextJS TicTacToe

Built as a test of different technologies and to explore real-time networking.

## Technologies Used

Bootstrap a developer-friendly NextJS app configured with:

- [Typescript](https://www.typescriptlang.org/)
- Linting with [ESLint](https://eslint.org/)
- Formatting with [Prettier](https://prettier.io/)
- Linting, typechecking and formatting on by default using [`husky`](https://github.com/typicode/husky) for commit hooks
- Testing with [Jest](https://jestjs.io/) and [`react-testing-library`](https://testing-library.com/docs/react-testing-library/intro)
- Pub/Sub model with [PubNub](https://pubnub.com)

## Deploy your own

Deploy the example using [Vercel](https://vercel.com):

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/import/project?template=https://github.com/vercel/next.js/tree/canary/examples/with-typescript-eslint-jest)

## How to use

### Create your .env.local

You will need to create your own `.env.local` with the follow shape:

```bash
NEXT_PUBLIC_PUBNUB_SUBSCRIBE_KEY="<YOUR_PUBLISH_KEY>"
NEXT_PUBLIC_PUBNUB_PUBLISH_KEY="<YOUR_SUBSCRIBE_KEY>"
```

### Run it locally

Install it and run with the following commands:

```bash
npm install
npm run dev
# or
yarn
yarn dev
```

### Deployment

Deploy it to the cloud with [Vercel](https://vercel.com/import?filter=next.js&utm_source=github&utm_medium=readme&utm_campaign=next-example) ([Documentation](https://nextjs.org/docs/deployment)).
