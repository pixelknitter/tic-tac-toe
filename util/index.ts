// Contains a set of utility tools

export enum BaseString {
  Lobby = 'tictactoelobby--',
  Game = 'tictactoegame--'
}

export enum ColorDefaults {
  ConfirmButton = 'rgb(11,102,35)',
  CancelButton = '#ddd',
  AlertText = 'rgb(208,33,41)'
}

export const __DEV__ = process.env.NODE_ENV === 'development'
export const __TEST__ = process.env.NODE_ENV === 'test'
export const __PROD__ = process.env.NODE_ENV === 'production'