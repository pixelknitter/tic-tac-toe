import React from 'react'

const Square: React.FC<React.HTMLProps<HTMLButtonElement>> = ({ value, onClick }) => (
  <button className="square" onClick={onClick}>
    {value}
    <style jsx>{`
      .square {
        background: #fff;
        border: 1px solid #999;
        float: left;
        font-size: 24px;
        font-weight: bold;
        line-height: 34px;
        height: 54px;
        margin-right: -1px;
        margin-top: -1px;
        padding: 0;
        text-align: center;
        width: 54px;
      }

      .square:focus {
        outline: none;
      }

      .square:hover {
        background: #eb9
      }

      .kbd-navigation .square:focus {
        background: #ddd;
      }
    `}</style>
  </button>
)

export default Square