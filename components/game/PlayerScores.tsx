import React from 'react'
import { ScoreContainer, PlayerContainer } from '../styled'
import { useGameContext, GameState } from "../contexts"

const PlayerScores = () => {
  const [state] = useGameContext()
  const { score } = state as GameState

  return (
    <ScoreContainer>
      <PlayerContainer>
        <h4>Player X</h4>
        <div>Score: {score.X}</div>
      </PlayerContainer>
      <PlayerContainer>
        <h4>Player O</h4>
        <div>Score: {score.O}</div>
      </PlayerContainer>
    </ScoreContainer>
  );
}

export { PlayerScores }