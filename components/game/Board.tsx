import React from 'react'
import Square from "./Square"
import { Piece } from "../contexts"
// import { GameConsumer } from './contexts'

interface BoardProps {
  disabled: boolean
  squares: Piece[]
  onClick: (location: number) => void
}

const Board: React.FC<BoardProps> = ({ disabled, squares, onClick }): JSX.Element => {
  // Create the 3 x 3 board
  const createBoard = (row: number, col: number): JSX.Element[] => {
    const board: JSX.Element[] = []
    let cellCounter = 0

    for (let i = 0; i < row; i += 1) {
      const columns = []
      for (let j = 0; j < col; j += 1) {
        columns.push(renderSquare(cellCounter++))
      }
      board.push(<div key={i} className="board-row">{columns}<style jsx>{`
        .board-row:after {
          clear: both;
          content: "";
          display: table;
        }
      `}</style></div>)
    }

    return board
  }

  const renderSquare = (index: number) => {
    return (
      <Square
        key={index}
        disabled={disabled}
        value={squares[index]}
        onClick={() => onClick(index)}
      />
    )
  }

  return (<div>{createBoard(3, 3)}</div>)
}

export { Board }