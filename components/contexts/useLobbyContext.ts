/* eslint-disable no-console */
import { useContext, useEffect } from 'react'
import { usePubNub } from 'pubnub-react'
import PubNub from "pubnub"
import shortId from 'shortid'
import Swal from 'sweetalert2'

import { LobbyState, LobbyDispatch } from './LobbyProvider'
import { BaseString, __DEV__ } from '../../util'

export interface LobbyActions {
  createLobby: () => Promise<{ roomId: string }>
  joinLobby: (roomId: string) => void
  resetLobby: () => void
  enableGame: () => void
}

export enum LobbyMessages {
  LobbyCreator = "LobbyCreator",
  GameActive = "gameActive"
}

function useLobbyStateContext() {
  const state = useContext(LobbyState)

  if (state === undefined) {
    throw new Error("Uh oh, we need a Lobby Provider! Missing the State.")
  }
  const { lobbyChannel } = state

  // get the pubnub client
  const client = usePubNub()
  if (client === undefined) { throw new Error("Uh oh, we are missing the Client!") }

  // listens to when the lobbyChannel state is updated
  useEffect(() => {
    // subscribe to the lobby channel
    if (lobbyChannel) {
      __DEV__ && console.log(`subscribe to the channel: ${lobbyChannel}`)
      client.subscribe({
        channels: [lobbyChannel],
        withPresence: true // Checks the number of people in the channel
      })
    }
    return () => {
      lobbyChannel && client.unsubscribe({
        channels: [lobbyChannel]
      })
    }
  }, [])

  // // identifies whom created the lobby
  // useEffect(() => {
  //   if (lobbyChannel) {
  //     client.publish({
  //       channel: lobbyChannel,
  //       message: {
  //         lobbyCreator: lobbyCreator,
  //       }
  //     })
  //   }
  // }, [lobbyCreator, lobbyChannel])

  return state
}

function useLobbyDispatchContext(): LobbyActions {
  // get the dispatch
  const dispatch = useContext(LobbyDispatch)
  if (dispatch === undefined) {
    throw new Error("Uh oh, we need a Lobby Provider! Missing the Dispatch.")
  }

  // get the pubnub client
  const client = usePubNub()
  if (client === undefined) { throw new Error("Uh oh, we are missing the Client!") }

  const createLobby = async (): Promise<{roomId: string}> => {
    // Create a random name for the channel
    const id = shortId.generate().substring(0,5)
    dispatch((draft) => {
      draft.roomId = id
      draft.lobbyCreator = true
      draft.lobbyChannel = BaseString.Lobby + id
      console.log(`set roomId: ${draft.roomId}`)
    })

    return new Promise((resolve) => {
      resolve({
        roomId: id
      })
    })
  }

  const joinLobby = async (newRoomId: string): Promise<PubNub> => {
    dispatch((draft) => {
      draft.roomId = newRoomId
      draft.lobbyChannel = BaseString.Lobby + newRoomId
      draft.lobbyCreator = false

      draft.lobbyChannel && client.hereNow({
        channels: [draft.lobbyChannel]
      }).then((response) => {
        if(response.totalOccupancy < 2) {
          console.log(`subscribe to lobby - ${draft.lobbyChannel}`)
          client.subscribe({
            channels: [draft.lobbyChannel],
            withPresence: true
          })
          client.publish({
            message: {
              lobbyCreator: false
            },
            channel: draft.lobbyChannel
          })
        } else{
          // Warning - game in progress
          Swal.fire({
            position: 'center',
            allowOutsideClick: false,
            title: 'Warning',
            text: 'Game in progress. Try another room.',
            width: 275,
            padding: '0.7em',
            customClass: {
                title: 'title-class',
                popup: 'popup-class',
                confirmButton: 'button-class'
            }
          })
        }
      }).catch((error) => {
        console.log(error)
      })
    })
    return client
  }

  const resetLobby = () => {
    dispatch((draft) => {
      draft.lobbyChannel = null
      draft.lobbyCreator = false
      draft.roomId = null
      draft.gameActive = false
    })
  }

  const enableGame = () => {
    dispatch(draft => {
      draft.gameActive = true
      if (draft.lobbyChannel) {
        console.log(`enable game`)
        client.publish({
          channel: draft.lobbyChannel,
          message: {
            gameActive: true
          }
        })
      }
    })
  }

  return { createLobby, joinLobby, resetLobby, enableGame }
}

// creates an array type of the dispatch and state for the lobby
const useLobbyContext = () => {
  return [useLobbyStateContext(), useLobbyDispatchContext()]
}

export { useLobbyContext }

// TODO: add the ability to customize your name
// /**
//    * displays a dialog asking for the users name and sets the lobby state.
//    * @param piece 
//    */
//   const askUserName = (piece: Piece) => {
//     Swal.fire<string>({
//       position: 'top',
//       input: 'text',
//       allowOutsideClick: false,
//       inputPlaceholder: 'Enter a name',
//       confirmButtonColor: ColorDefaults.ConfirmButton,
//       confirmButtonText: 'OK',
//       width: 275,
//       padding: '0.7em',
//       customClass: {
//         popup: 'popup-class',
//         confirmButton: 'join-button-class',
//         cancelButton: 'join-button-class'
//       }
//     }).then((result) => {
//       // Check if the user typed a value in the input field
//       if (result.value) {
//         switch(piece) {
//           case Piece.O:
//             setUserNameO(name)
//             // TODO: publish username to opponent
//             break
//           case Piece.X:
//             setUserNameX(name)
//             // TODO: publish username to opponent
//             break
//         }
//       }
//     })
//   }