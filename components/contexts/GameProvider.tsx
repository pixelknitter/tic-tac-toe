/* eslint-disable no-console */
import React from 'react'
import { Piece } from "."
import { useImmer } from "use-immer"

/**
 * Game Interfaces and Types
 */

type GameSession = {
  gameChannel: string
}

interface GameState extends GameSession {
  playerPiece: Piece
  squares: Piece[]
  turnCounter: number
  endgame: boolean
  score: Score
}

export const initialGameState: GameState = {
  playerPiece: Piece.X,
  gameChannel: null,
  squares: [],
  turnCounter: 0,
  endgame: false,
  score: { X: 0, O: 0 }
}

export type Score = {
  X: number
  O: number
}

export type GameDispatchFunction = (f: (draft: {
  playerPiece: Piece
  squares: Piece[]
  turnCounter: number
  endgame: boolean
  score: Score
  gameChannel: string
}) => void | GameState) => void

const GameState = React.createContext({} as GameState)
const GameDispatch = React.createContext({} as GameDispatchFunction)

const GameProvider = (props) => {
  const [state, dispatch] = useImmer<GameState>({ ...initialGameState })

  return (
  <GameState.Provider value={state}>
    <GameDispatch.Provider value ={dispatch}>
      {props.children}
    </GameDispatch.Provider>
  </GameState.Provider>)
}

export { GameProvider, GameState, GameDispatch };