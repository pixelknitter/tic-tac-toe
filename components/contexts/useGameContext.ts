import { initialGameState } from "./GameProvider"
/* eslint-disable no-console */
import { useContext, useEffect } from "react"
import Swal from 'sweetalert2'

import { useLobbyContext, LobbyActions } from "./useLobbyContext"
import { LobbyValues, Piece } from "./LobbyProvider"
import { BaseString, ColorDefaults } from "../../util"
import { GameState, GameDispatch, Score } from './GameProvider'
import { usePubNub } from "pubnub-react"

/**
 * Game Interfaces and Types
 */
export interface GameActions {
  resetGame: () => void
  startGame: (newGameRoomId: string) => void
  conductMove: (location: number) => void
  receiveMove: (player: Piece, location: number) => void
  checkForWinner: (squares: Piece[]) => void
  newGame: () => void
}

export enum GameMessages {
  LobbyCreator = "lobbyCreator",
  MoveLocation = "moveLocation",
  PlayerPiece = "playerPiece",
  NewGame = "newGame",
  EndGame = "endGame",
  ReturnToLobby = "returnToLobby",
  UpdateScore = "updateScore",
}

interface NewGameUpdate {
  startGame: boolean
  score: Score
  previousWinner: Piece
}

/**
 * Helper Functions
 */
export const decideTurn = (turnCounter: number): Piece => {
  return (turnCounter % 2 === 0) ? Piece.X : Piece.O
}

const useGameState = (): GameState => {
  const gameState = useContext(GameState)

  // ensure we're in the right providers
  if (gameState === undefined) {
    throw new Error("Game State is missing. It must be used within a Game Provider!")
  }

  const client = usePubNub()
  if (client === undefined) {
    throw new Error("Client is missing. It must be within an appropriate Provider!")
  }

  return { ...gameState }
}

const useGameDispatch = (): GameActions => {
  const gameDispatch = useContext(GameDispatch)
  const gameState = useGameState()
  const [lobbyState, lobbyDispatch] = useLobbyContext()

  if (lobbyState === undefined) {
    throw new Error("Lobby State is missing. It must be used within a Lobby Provider!")
  }
  if (gameDispatch === undefined) {
    throw new Error("Game Dispatch is missing. It must be used within a Game Provider!")
  }

  const client = usePubNub()
  if (client === undefined) {
    throw new Error("Client is missing. It must be within an appropriate Provider!")
  }

  const { lobbyChannel, roomId, lobbyCreator, gameActive } = lobbyState as LobbyValues
  const { resetLobby, enableGame } = lobbyDispatch as LobbyActions
  // const { gameChannel, endgame, squares, playerPiece, turnCounter } = gameState

  // TODO: refactor these different states into a reducer

  // reset the subscriptions and listeners at the beginning
  useEffect(() => {
    console.info(`resetting everying for a new session`)
    resetGame()
  }, [])

  // check if another player has joined the lobby
  // if so, create a game channel and subscribe to it. Then start game
  useEffect(() => {
    // guards against the times when we reset the lobby
    let interval
    console.debug(`${(lobbyCreator) ? 'I am the lobby creator' : 'I am not the lobby creator'}`)
    if (lobbyChannel) {
      // only launch if you're the guest
      if (!gameActive && !lobbyCreator) {
        Swal.fire({
          position: 'top',
          allowOutsideClick: false,
          title: 'Prepare for Battle',
          text: 'Waiting for the game to connect...',
          confirmButtonColor: ColorDefaults.ConfirmButton,
          width: 275,
          customClass: {
              title: 'title-class',
              popup: 'popup-class',
              confirmButton: 'button-class',
          },
        })
      } // only launch this if you're a host
      (!gameActive && lobbyCreator) && Swal.fire({
        position: 'top',
        allowOutsideClick: false,
        title: `Prepare for Battle`,
        text: `Share your code - ${roomId}`,
        width: 275,
        customClass: {
            title: 'title-class',
            popup: 'popup-class',
        },
      })
      interval = setInterval(() => {
        console.log('Waiting for opponent to connect...')
        lobbyChannel && client.hereNow({
          channels: [lobbyChannel]
        }).then((response) => {
          if(response.totalOccupancy == 2) {
            console.log(`${roomId} has enough players to play!`)
            startGame(roomId)
          }
        })
      }, 3000)
    }
    gameActive && clearInterval(interval)
    return () => { clearInterval(interval)
      console.log(`remove listener ${listener}`)
      client.removeListener(listener)
    }
  }, [gameActive])

  // create game channel, subscribe
  // publish name, playerPiece
  // add listener for messages
  const listener = {
    message(msg) {
      const room = msg.subscribedChannel
      const { message } = msg
      console.log(`current message from ${room}: ${JSON.stringify(message)}`)
      // make sure we're in the right space
      if(room === BaseString.Game + roomId) {
        // actions for each GameMessage type
        if (GameMessages.PlayerPiece in message
          && GameMessages.MoveLocation in message
          ) {
            const location = message[GameMessages.MoveLocation]
            const receivedPiece = message[GameMessages.PlayerPiece]
            console.debug(`move received: ${(lobbyCreator) ? 'I am the lobby creator' : 'I am not the lobby creator'}`)
            console.log(`received piece: ${receivedPiece} should not be ${gameState.playerPiece}`)
            // update the board from your opponent
            if (gameState.playerPiece !== receivedPiece) {
              receiveMove(receivedPiece, location)
            }
        }
        else if (GameMessages.LobbyCreator in message) {
          const receivedCreator = message[GameMessages.LobbyCreator]
          console.log(`${receivedCreator ?? "RECEIVED LOBBY"}`)
          if (!receivedCreator) {
            console.log('unsubscribe all channels')
              // reset to ensure we only subscribe to one
              client.unsubscribeAll()
            gameDispatch(draft => {
              draft.gameChannel = BaseString.Game + roomId
            })
            console.log(`re-subscribe to ${gameState.gameChannel} and ${lobbyChannel}`)
            client.subscribe({
              channels: [gameState.gameChannel, lobbyChannel]
            })
          }
        }
        else if (GameMessages.ReturnToLobby in message) {
          // end the game
          if (message[GameMessages.ReturnToLobby]) {
            Swal.close()
            resetLobby()
            resetGame()
          }
        }
        else if (GameMessages.NewGame in message) {
          const newGameUpdate: NewGameUpdate = message[GameMessages.NewGame]
          if (newGameUpdate.startGame) {
            Swal.close()
            if (newGameUpdate.previousWinner) {
              console.debug(`the new score: ${newGameUpdate.score}`)
            }
            newGame()
          }
        }
        else if (GameMessages.EndGame in message) {
          console.log("run the end game...")
          if (message[GameMessages.EndGame]) {
            if (!gameState.endgame) {
              gameDispatch(draft => {
                draft.endgame = true
              })
            }
          }
        }
      }
      // else { // otherwise get out!
      //   console.log(`message received, unsubscribe from ${gameChannel} & ${lobbyChannel}`)
      //   resetLobby()
      //   resetGame()
      //   const newGameChannel = BaseString.Game + room
      //   const newLobbyChannel = BaseString.Lobby + room
      //   console.log(`message received, resubscribed to ${newGameChannel} & ${newLobbyChannel}`)
      //   joinLobby(room)
      //   startGame(room)
      // }
    }
  }

  // MARK - actions

  /**
   * if not end of game, transitions to the next round. Otherwise displays an alert and asks
   * if the players would like to continue playing.
   * @param winner - the X or O player
   */
  const transitionRound = (winner: Piece | null) => {
    console.log(`Transition the rounder w/ winner: ${winner}`)
    // Announce the winner or announce a tie game
    const title = (winner === null) ? 'Tie game!' : `Player ${winner} won!`;
    // Show this to Player O
    if (!lobbyCreator && gameState.endgame) {
      Swal.fire({
        position: 'top',
        allowOutsideClick: false,
        title: title,
        text: 'Waiting for a new round...',
        confirmButtonColor: ColorDefaults.ConfirmButton,
        width: 275,
        customClass: {
            title: 'title-class',
            popup: 'popup-class',
            confirmButton: 'button-class',
        } ,
      })
    }
    // Show this to Player X
    else if (lobbyCreator && gameState.endgame) {
      Swal.fire({
        position: 'top',
        allowOutsideClick: false,
        title: title,
        text: 'Continue Playing?',
        showCancelButton: true,
        confirmButtonColor: ColorDefaults.ConfirmButton,
        cancelButtonColor: ColorDefaults.CancelButton,
        cancelButtonText: 'Nope',
        confirmButtonText: 'Yea!',
        width: 275,
        customClass: {
            title: 'title-class',
            popup: 'popup-class',
            confirmButton: 'button-class',
            cancelButton: 'button-class'
        } ,
      }).then((result) => {
        // Start a new round
        if (result.value) {
          gameState.gameChannel && client.publish({
            message: {
              newGame: {
                startGame: true,
                score: winner,
                previousWinner: winner
              }
            },
            channel: gameState.gameChannel
          })
        } else {
          // Kick everyone to the lobby
          gameState.gameChannel && client.publish({
            message: {
              returnToLobby: true
            },
            channel: gameState.gameChannel
          })
        }
      })
    }
  }

  const checkForWinner = (squares: Piece[]) => {
    console.log(`checking for winners w/ ${squares}`)
    // Possible winning combinations
    const possibleCombinations = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    // Iterate every combination to see if there is a match
    for (let i = 0; i < possibleCombinations.length; i += 1) {
      const [a, b, c] = possibleCombinations[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        announceWinner(squares[a])
        return
      }
    }
      // The board is filled up and there is no winner
      if(gameState.turnCounter === 9){
        gameDispatch(draft => {
          draft.endgame = true
        })
        transitionRound(null)
      }
  }

  // Update score for the winner
  const announceWinner = (winner: Piece) => {
    gameDispatch(draft => {
      draft.score[winner] += 1
      // End the game once there is a winner
      draft.endgame = true
    })
    transitionRound(winner)
  }

  // Perform a move that is sent out
  const conductMove = (location: number) =>{
    // Check if the square is empty and if it's the player's turn to make a move
    console.log(`location: ${location} currentTurn: ${decideTurn(gameState.turnCounter)}, playerPiece ${gameState.playerPiece}`)
    if(!gameState.squares[location] && (decideTurn(gameState.turnCounter) === gameState.playerPiece)) {
      gameDispatch(draft => {
        draft.turnCounter += 1
        draft.squares[location] = gameState.playerPiece
      })
      // Other player's turn to make a move
      console.log(`the current turn after decided (conduct move): ${decideTurn(gameState.turnCounter)}`)
      // Publish move to the channel
      try {
        gameState.gameChannel && client.publish({
          channel: gameState.gameChannel,
          message: {
            moveLocation: location,
            playerPiece: gameState.playerPiece,
          }
        })
      } catch (e) { console.error(`an error occurred: ${e}`)}
      // check if there's a winner
      // checkForWinner(squares)
    }
  }

  // Opponent's move is published to the board
  const receiveMove = (piece: Piece, location: number) => {
    console.log(`received a move: ${piece} at ${location}, anything there? ${!gameState.squares[location]}`)
    if(!gameState.squares[location]) {
      gameDispatch(draft => {
        // update the squares
        draft.turnCounter += 1
        draft.squares[location] = piece
      })
      // toggle the current turn
      // checkForWinner(squares)
    }
  }

  // reset everything
  const resetGame = () => {
    // unsubscribe from channels
    // client.subscribe({
    //   channels: [gameChannel, lobbyChannel]
    // })
    console.log('reset game')
    client.removeListener(listener)
    gameState.gameChannel && client.unsubscribeAll()
    gameDispatch(draft => {
      draft.gameChannel = initialGameState.gameChannel
      draft.endgame = initialGameState.endgame
      draft.turnCounter = initialGameState.turnCounter
      draft.squares = initialGameState.squares
      draft.score = initialGameState.score
    })
  }

  const newGame = () => {
    console.log('start new game')
    gameDispatch(draft => {
      // reset some of the state
      draft.endgame = false
      draft.turnCounter = 0
      draft.squares = []
    })
    startGame(roomId)
  }

  // start a new game
  const startGame = (newGameRoomId: string) => {
    console.log(`start game in ${newGameRoomId}`)
    enableGame()
    gameDispatch((draft) => {
      draft.playerPiece = lobbyCreator ? Piece.X : Piece.O
      draft.gameChannel = BaseString.Game + newGameRoomId
      gameState.gameChannel && console.log(`unsubscribe from the channel: ${gameState.gameChannel}`)
      gameState.gameChannel && client.unsubscribe({ channels: [gameState.gameChannel]})
      console.log(`subscribe to the channel: ${draft.gameChannel} in the lobby ${lobbyChannel}`)
      draft.gameChannel && client.subscribe({
        channels: [lobbyChannel, draft.gameChannel]
      })
      console.log(`add listener for game messages`)
      client.addListener(listener)
    })
    Swal.close() // makes sure we close any pop-ups
  }

  return { conductMove, receiveMove, resetGame, startGame, newGame, checkForWinner }
}

// creates an array type of the dispatch and state for the lobby
const useGameContext = () => {
  return [useGameState(), useGameDispatch()]
}

export { useGameContext }