/* eslint-disable no-console */
import React from 'react'
import { useImmer } from 'use-immer'

export enum Piece {
  X = "X",
  O = "O"
}

export interface LobbyValues {
  roomId: string
  lobbyChannel: string
  lobbyCreator: boolean
  gameActive: boolean
}

// TODO: add this to the lobby state
// userNameX: string
// userNameO: string

export const initialLobbyState: LobbyValues = {
  roomId: null,
  lobbyChannel: null,
  lobbyCreator: false,
  gameActive: false,
}

export type DispatchFunction = (f: (draft: {
  roomId: string
  lobbyChannel: string
  lobbyCreator: boolean
  gameActive: boolean
}) => void | LobbyValues) => void

const LobbyState = React.createContext({} as LobbyValues)
const LobbyDispatch = React.createContext({} as DispatchFunction)

const LobbyProvider = ({ children }) => {
  const [state, dispatch] = useImmer<LobbyValues>({ ...initialLobbyState })

  return (
    <LobbyState.Provider value={state}>
      <LobbyDispatch.Provider value={dispatch}>
        {children}
      </LobbyDispatch.Provider>
    </LobbyState.Provider>
  )
}

export { LobbyProvider, LobbyState, LobbyDispatch }
