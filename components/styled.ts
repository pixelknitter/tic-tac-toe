import styled from 'styled-components'

export const ScoreContainer = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin-top: 1em;
  border-top: 1px dotted darkgray;
`

export const PlayerContainer = styled.div`
  padding: 1em;
  margin-top: 1em;
`

export const Button = styled.button`
  margin-top: 1em;
  font-size: 1em;
  padding: 0.5em;
  min-height: 40px;
  border-radius: 0.25em;
  cursor: pointer;
`

export const BoardRow = styled.div`
:after {
  clear: both;
  content: "";
  display: table;
}
`