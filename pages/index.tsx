import React, { useEffect } from 'react'
import Head from 'next/head'
import Swal from 'sweetalert2'
import Router, { useRouter } from 'next/router'
import { useLobbyContext, LobbyActions, LobbyValues } from '../components'
import { Button } from "../components"

const routeToRoom = (roomId: string) => {
  Router.push('/session/[roomId]', `/session/${roomId}`)
}

const onPressCreate = (roomId: string) => {
  return Swal.fire({
    position: 'center',
    allowOutsideClick: false,
    title: 'Share this room ID with your friend',
    text: roomId,
    width: 275,
    padding: '0.7em',
    // Custom CSS to change the size of the modal
    customClass: {
        title: 'title-class',
        popup: 'popup-class',
        confirmButton: 'button-class'
    }
  }).then(() => {
    routeToRoom(roomId)
  })
}

const onPressJoin = (joinRoom: (roomId: string) => void) => {
  return Swal.fire<string>({
    position: 'center',
    input: 'text',
    allowOutsideClick: false,
    inputPlaceholder: 'Enter the room id',
    showCancelButton: true,
    confirmButtonColor: 'rgb(11,102,35)',
    confirmButtonText: 'OK',
    width: 275,
    padding: '0.7em',
    customClass: {
      popup: 'popup-class',
      confirmButton: 'join-button-class',
      cancelButton: 'join-button-class'
    }
  }).then((result) => {
    // Check if the user typed a value in the input field
    if (result.value && result.value.length === 5) {
      joinRoom(result.value)
      routeToRoom(result.value)
    }
  })
}

export const Home = (): JSX.Element => {
  const router = useRouter()
  const [state, dispatch] = useLobbyContext()
  const { roomId } = state as LobbyValues
  const { createLobby, joinLobby } = dispatch as LobbyActions

  useEffect(() => {
    // Prefetch the session page as the user will go there after the lobby creation/join
    router.prefetch('/session/[roomId]', `/session/[roomId]`)
  }, [roomId])

  return (<div className="container">
  <Head>
    <title>Tic Tac Toe</title>
    <link rel="icon" href="/favicon.ico" />
  </Head>

  <main>
    <h1 className="title">
      Tic Tac Toe
    </h1>

    <p className="description">
      Get started by creating or joining a session! &rarr;
    </p>

    <div className="buttonBox">
      <Button
        onClick={() => {
          createLobby().then((result) => {
            onPressCreate(result.roomId) })
        }}
      >
        Create
      </Button>
      <Button
        onClick={() => { onPressJoin(joinLobby) }}>
        Join
      </Button>
    </div>
  </main>

  <footer>
    <a
      href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
      target="_blank"
      rel="noopener noreferrer"
    >
      Powered by <img src="/vercel.svg" alt="Vercel Logo" className="logo" />
    </a>
    &nbsp;&&nbsp;
    <a
      href="https://www.pubnub.com/"
      target="_blank"
      rel="noopener noreferrer"
    >
      <img src="/pubnub-logo.png" alt="Pubnub Logo" className="logo" />
    </a>
  </footer>

  <style jsx>{`
    .container {
      min-height: 100vh;
      padding: 0 0.5rem;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    main {
      padding: 5rem 0;
      flex: 1;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    footer {
      width: 100%;
      height: 100px;
      border-top: 1px solid #eaeaea;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    footer img {
      margin-left: 0.5rem;
    }

    footer a {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    a {
      color: inherit;
      text-decoration: none;
    }

    .title a {
      color: #0070f3;
      text-decoration: none;
    }

    .title a:hover,
    .title a:focus,
    .title a:active {
      text-decoration: underline;
    }

    .title {
      margin: 0;
      line-height: 1.15;
      font-size: 4rem;
    }

    .title,
    .description {
      text-align: center;
    }

    .description {
      line-height: 1.5;
      font-size: 1.5rem;
    }

    .buttonBox {
      display: flex;
      justify-content: space-between;
      width: 124px;
      max-width: 800px;
    }

    .logo {
      height: 1em;
    }

    @media (max-width: 600px) {
      .buttonBox {
        display: flex;
        justify-content: space-between;
        height: 60px;
        flex-direction: column;
      }
    }
  `}</style>

  <style jsx global>{`
    html,
    body {
      padding: 0;
      margin: 0;
      font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
        Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
    }

    * {
      box-sizing: border-box;
    }
  `}</style>
  </div>)
}

export default Home
