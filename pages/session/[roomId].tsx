/* eslint-disable no-console */
import { useState, useEffect } from "react"
import { useRouter } from 'next/router'
import Head from 'next/head'

import { Board, useLobbyContext, LobbyValues, useGameContext,
  GameActions, GameState, LobbyActions, Button } from "../../components"
import { PlayerScores } from "../../components/game"
import { decideTurn } from "../../components/"

const Session = () => {

  const [state, lobbyDispatch] = useLobbyContext()
  const { resetLobby } = lobbyDispatch as LobbyActions
  const { roomId, gameActive } = state as LobbyValues
  const [gameState, gameDispatch] = useGameContext()
  const { conductMove, resetGame, checkForWinner } = gameDispatch as GameActions
  const { squares, turnCounter, playerPiece, endgame } = gameState as GameState

  const router = useRouter()
  // const { roomId: queryId } = router.query

  const handleExit = () => {
    router.push('/')
    resetLobby()
    resetGame()
  }

  const myTurn = decideTurn(turnCounter) === playerPiece
  const displayTurn = () => {
    return `${myTurn ?
      `Your move, Player ${playerPiece}` :
      `Their move, Player ${playerPiece}`}`
  }

  const [status, setStatus] = useState<string>(displayTurn())
  // update turn status
  useEffect(() => {
    setStatus(displayTurn())
    console.debug(`who's up? ${status}, player is ${playerPiece}`)
  }, [turnCounter, playerPiece])

  // useEffect(() => {
  //   // try to join the lobby from the URI
  //   if (queryId && queryId.length === 5) {
  //     console.log('lets try to join from the URI')
  //     joinLobby(queryId as string)
  //   }
  // }, [])
  // TODO: if session is missing a lobby, create one (ex: when coming to link direcly)

  // TODO: ask for username on first entry

  // Check if there is a winner
  useEffect(() => {
    if (gameActive && !endgame) {
      checkForWinner(squares)
    }
  }, [squares])

  return (<div>
      <Head>
        <title>Tic Tac Toe - Room |{roomId}|</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
      <h1 className="title">
        Room Code - {roomId}
      </h1>
      <Button onClick={handleExit}>Exit Room</Button>
      <div className="game">
        <Board
          disabled={gameActive && !myTurn}
          squares={squares}
          onClick={index => conductMove(index)}
        />
        <p className="status-info">{status}</p>
        <p className="status-info">{`TURN ${turnCounter}`}</p>
        <PlayerScores />
      </div>
      </main>
      <style jsx>{`
      .container {
        min-height: 100vh;
        padding: 0 0.5rem;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      main {
        padding: 5rem 0;
        flex: 1;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }

      .board-container {
        display: flex;
        flex-direction: row;
        width: 16.5em;
      }

      .game {
        display: flex;
        flex-direction: column;
        margin-top: 1.2em;
        justify-content: center;
        align-items: center;
        min-height: 15vh;
      }

      .status-info {
        margin-left: 20px;
        color: rgb(208,33,41);
      }

      .scores-container {
        margin-right: 10em;
        color: rgb(208,33,41);
      }
    `}</style>
  </div>)
}

export default Session