import React from 'react'
// import App from 'next/app'
import PubNub, { generateUUID } from 'pubnub'
import { PubNubProvider } from 'pubnub-react'
import { GameProvider, LobbyProvider } from "../components";

const pubNubClient = new PubNub({
    publishKey: process.env.NEXT_PUBLIC_PUBNUB_PUBLISH_KEY,
    subscribeKey: process.env.NEXT_PUBLIC_PUBNUB_SUBSCRIBE_KEY,
    uuid: generateUUID()
})

// import '../styles.css' // add global styles

function Game ({ Component, pageProps }) {
    // make sure the pubNub client exists
    if (!pubNubClient) { return } // <Error>Oops! We've had an error.</Error>

    return (
        <PubNubProvider client={pubNubClient}>
            <LobbyProvider>
                <GameProvider>
                    <Component {...pageProps} />
                </GameProvider>
            </LobbyProvider>
        </PubNubProvider>)
}

export default Game;